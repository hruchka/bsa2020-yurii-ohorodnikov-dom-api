const search = document.getElementById("search");
const matchList = document.getElementById("match-list");
const ascendingBtn = document.getElementById("sort-asc");
const descendingBtn = document.getElementById("sort-desc");
const popularBtn = document.getElementById("sort-popular");
const url = 'https://rickandmortyapi.com/api/character';

document.addEventListener('DOMContentLoaded', async () => {
  search.addEventListener('input', getMatchedResults);
  ascendingBtn.addEventListener('click', getNewestResults);
  descendingBtn.addEventListener('click', getLatestResults);
  popularBtn.addEventListener('click', getPopularResults);
  
  const data = await getData(url);
  let characters = data.results;
  let results = characters;
  showResults(results);

  function showResults(results) {
    matchList.innerHTML = '';
    results.forEach(item => {
      const container = createElem({tag: 'div', root: matchList, prop: {'class': "card card-body mb-3"}});
      createElem({tag: 'h4', root: container, prop: {'class': 'text-primary'}, text: item.name});
      createElem({tag: 'img', root: container, prop: {'src': item.image}});
      createElem({tag: 'p', root: container, text: `Species: ${item.species}`});
      createElem({tag: 'p', root: container, text: `Last known location: ${item.location.name}`});
      createElem({tag: 'p', root: container, text: `Created: ${new Date(item.created).toLocaleDateString("en-US")}`});
      const episodeContainer = createElem({tag: 'div', root: container});
      item.episode.forEach(async item => {
        const res = await getData(item);
        createElem({tag: 'p', root: episodeContainer, text: res.name})
      })
      const deleteBtn = createElem({tag: 'button', root: container, prop: {'class': "btn btn-danger mb-2", 'id':"delete"}, text: "Delete"});
      deleteBtn.addEventListener('click', deleteCharacter)
    })
  }

  function getMatchedResults(e) {
    const value = e.target.value;
    results = characters.filter(character => {
      const regex = new RegExp(`^${value}`, "gi");
      return character.name.match(regex);
    });
    showResults(results);

  }

  function getNewestResults(e) {
    results = [...characters].sort((a,b) => {
      return new Date(a.created) > new Date(b.created) ? -1 : 1;
    });
    showResults(results);
  }

  function getLatestResults(e) {
    results = [...characters].sort((a,b) => {
      return new Date(a.created) > new Date(b.created) ? 1 : -1;
    });
    showResults(results);
  }

  function getPopularResults(e) {
    results = [...characters].sort((a,b) => {
      if (a.episode.length > b.episode.length) return -1;
      if (a.episode.length === b.episode.length) {
        return new Date(a.created) > new Date(b.created) ? -1 : 1;
      } else {
        return 1;
      }
    });
    showResults(results);
  }

  function deleteCharacter(e) {
    const name = e.target.parentNode.firstChild.innerText;
    characters = characters.filter(item => item.name !== name);
    e.target.parentNode.remove();
  }
  

  function createElem({tag, root, prop, text}) {
    const elem = document.createElement(tag);
    if (prop) {
      for (let key in prop) {
        elem.setAttribute(key, prop[key])
      }
    }
    if (text) {
      elem.innerText = text;
    }
    root.appendChild(elem);
    return elem
  }

  async function getData(url) {
    try {
      const res = await fetch(url);
      const data = await res.json();
      return data
    }
    catch (err) {
      return console.log(err);
    }
  }
})